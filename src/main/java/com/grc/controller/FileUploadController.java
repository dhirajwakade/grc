package com.grc.controller;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FileUploadController 
{
	@RequestMapping("/fileUploadPage")
	public String fileUpload()
	{
		return "fileUploadPage";
	}
	@RequestMapping(value="/fileUpload",method=RequestMethod.POST)
	public ModelAndView upload(@RequestParam("file")MultipartFile file,HttpSession session)
	{
		String filename=uploadImg(file,session);
		System.out.println("==========filen nam="+filename);
		return new ModelAndView("fileUploadPage","filename",filename);
	}
	public String uploadImg(MultipartFile file,HttpSession session)
	{
		int flg=0;
		String path=session.getServletContext().getRealPath("/");  
        String filename=file.getOriginalFilename();
        path=path.concat("resources/UploadFiles");
        try
        {  
	        byte barr[]=file.getBytes();  
	        System.out.println("=====path=="+path+"/"+filename);
	        BufferedOutputStream bout=new BufferedOutputStream(new FileOutputStream(path+"/"+filename));  
	        bout.write(barr);
	        bout.flush();  
	        bout.close();
        }
        catch(Exception e)
        {
        	flg++;
        	System.out.println(e);
        }
        if(flg==0)
        return filename;
        else
        return "not"; 
	}
	@RequestMapping("CreateResult")
	public String createResult()
	{
		String str="{JSON=[{'id':1,'label':'person','object':'kernel'},{'id':2,'label':'ahv','object':'attribute','master':{'id':1,'label':'person'},'type':'serial','expression':'','primaryKey':true},{'id':3,'label':'firstname','object':'attribute','master':{'id':1,'label':'person'},'type':'varchar(128)','expression':'default unknown','primaryKey':false}]}";		
		try 
		{
			JSONObject json=new JSONObject(str);
			JSONArray jsonArray = json.getJSONArray("JSON");
			Map<String, String>tables=new LinkedHashMap<String, String>();
	        Map<String, Map<String,String>> mapAttr = new LinkedHashMap<String, Map<String,String>>();
			for (int i = 0; i < jsonArray.length(); i++) 
		    {
				try
				{
			        JSONObject explrObject = jsonArray.getJSONObject(i);
			        String objstr=explrObject.getString("object");
			        switch(objstr)
			        {
			        	case "kernel":tables.put(String.valueOf(explrObject.getInt("id")),explrObject.getString("label"));break;
			        	
			        	case "attribute":	LinkedHashMap<String, String>attr=new LinkedHashMap<String, String>();
			        						attr.put("id",String.valueOf(explrObject.getInt("id")));
			        						attr.put("label",explrObject.getString("label"));
			        						attr.put("object",explrObject.getString("object"));
			        						attr.put("type",explrObject.getString("type"));
			        						attr.put("expression",explrObject.getString("expression"));
			        						attr.put("primaryKey",String.valueOf(explrObject.get("primaryKey")));
			        						attr.put("fid",String.valueOf(explrObject.getJSONObject("master").getInt("id")));
			        						mapAttr.put(String.valueOf(explrObject.getInt("id")),attr);
			        						break;
			        						
			        }
				}
				catch(JSONException ee)
				{
				}
		    }
			String sql="CREATE TABLE ";
			for(Map.Entry<String, String> entry : tables.entrySet())
			{
			    System.out.println(entry.getKey()+" "+entry.getValue());
			    sql=sql.concat(entry.getValue()+"(");
				for(Map.Entry<String, Map<String,String>> entryAttr : mapAttr.entrySet())
				{
				    Map<String,String>m=mapAttr.get(entryAttr.getKey());
				    for(Map.Entry<String, String> entrys :m.entrySet())
					{
					    System.out.println(entrys.getKey()+" "+entrys.getValue());
					    if(entrys.getKey().equalsIgnoreCase("fid"))
					    {
					    	if(Integer.parseInt(m.get("fid"))==Integer.parseInt(entry.getKey()))
					    	{
					    		sql=sql.concat(m.get("label")+" "+m.get("type")+" "+m.get("expression"));
					    		if(m.get("primaryKey").equalsIgnoreCase("true"))
					    		{
					    			sql=sql.concat(" primary key,");
					    		}
					    	}
					    }
					}
				}
			}
			sql=sql.concat(")");
			System.out.println("sql======"+sql);
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		return "fileUploadPage";
	}
	public String WriteOnFile()
	{
		return "fileUploadPage";
	}
}
